# README #

### What is this repository for? ###

* Matlab implementation of VideoDarwin.
* Version 1.0
* [CVPR paper](http://www.cv-foundation.org/openaccess/content_cvpr_2015/html/Fernando_Modeling_Video_Evolution_2015_CVPR_paper.html)

### Dependency ###

* vlfeat-0.9.18
* opencv-2.4.9
* liblinear-1.93
* libsvm-3.18
* improved trajectory implementation (binary)


### Contact ###

* Basura Fernando 
* http://users.cecs.anu.edu.au/~basura/


### LICENSE & TERMS OF USE
----------------------
VideoDarwin code implements a sequence representation technique.
Copyright (C) 2015  Basura Fernando


### Terms of Use
--------------
This VideoDarwin software is strictly for non-commercial academic use only. 
This VideoDarwin code or any modified version of it may not be used for any commercial activity, such as:
1. Commercial production development, Commercial production design, design validation or design assessment work.
2. Commercial manufacturing engineering work
3. Commercial research.
4. Consulting work performed by academic students, faculty or academic account staff
5. Training of commercial company employees.

### License
-------
The analysis work performed with the program(s) must be non-proprietary work. 
Licensee and its contract users must be or be affiliated with an academic facility. 
Licensee may additionally permit individuals who are students at such academic facility 
to access and use the program(s). Such students will be considered contract users of licensee. 
The program(s) may not be used for commercial competitive analysis 
(such as benchmarking) or for any commercial activity, including consulting.